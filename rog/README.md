# Pieces related to CentOS Stream and RHEL dist-git workflow on GitLab

## Base pipeline implementation plan

* Submit a draft build.
* A job per each common task that has to run on every MR, dependent on the
  draft build job.
* Job (also depending on the draft build) that processes the per-project gating
  tests and creates a [dynamic pipeline] with all these tests and result tracking
  for them. A python script can be used to implement the dynamic pipeline generation.
  This requires all relevant test metadata to be present in the project, e.g.
  in a `gating.yaml` file or appropriate tmt/fmf files that are machine parseable.
  The format needs to be known in advance as the script needs to be able to parse
  the metadata, the locations should be possible to configure if needed.
* Optional tests should be marked as manual jobs so that maintainers can trigger
  them only when they need them.
* Tests that are not required to pass (e.g. are being currently onboarded or
  aren't marked as gating) should be marked as allowed to fail.
* A dummy query job should be created as a catch-all for test results that are
  submitted against the build but are not mentioned in the gating file.

For now, the plan for the test jobs is to track test status via API queries to
the result dashboard. If the GitLab job is restarted, it should also restart
the executor that runs the test.

Later on, the common tests may get reimplemented to run directly in GitLab CI
to simplify this flow, and only keep the runs from external CI systems in the
query approach.

[dynamic pipeline]: https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#dynamic-child-pipelines

## Waiving (manual and automatic) and automatic restarts 

Automatic waiving and restarts should be considered. If a test failure pattern
is marked as autorestart, the match should be logged and test should be restarted.
If a test failure pattern is marked as autowaive, the match should be logged
and the failure ignored.

Creating a waive pattern shouldn't do anything by default, but in the future
we may want to consider adding a checkbox to allow querying past results. This
should be considered based on usefulness of the feature.

Automatic waive and restart pattern permissions should be very carefully
considered and consulted between test and package maintainers. They should only
be created for sufficiently split test results (subtasks), as we don't want to
for example automatically waive a full test execution with multiple failures
where one of the failures is known and one is new. We **DO NOT** want to miss
new failures by accident here!

Manual result waiving by maintainers should be possible for the regular flow.

Waiving a test result (automatically or manually) should not restart the test
nor build.

It should be easy to understand how to waive the results from the MR, as we
want to minimize interaction of developers and maintainers with tooling outside
of the MR and Jira.

## Considerations

### Pipelines must succeed in order to merge the MRs

This is good as it prevents merging bugs accidentally, but it also means we
need to think about how to implement waiving while not making test results too
confusing.

It is not possible to continue the pipeline if a job fails unless that job is
marked as allowed to fail. Thus, we cannot "truly" fail the failed tests and
then have a result checking job that waives the results and passes. To do this,
we'd have to mark all test jobs as allowed to fail, which then gets confusing
with the optional tests that are actually allowed to fail. Thus, we need a
different approach.

A workaround is to implement something similar to what CKI pipelines do: have
the test job status refer to the status of the test *execution* as opposed to
the test *result*, i.e. information about whether the test managed to complete.
Then, we can have a result status job that gathers all information about the
test run results and presents them. Links to waive and restart the tests can be
provided here. The result job can exit with a failure and thus prevent merging,
or exit with allowed failure [if only optional tests failed].

### Waiving should not restart tests

Slightly related to the previous point: if we don't want the waiving of results
to restart the tests, we cannot have test execution and result status in a single
GitLab job. The solution for the previous point fixes this problem as well:

1. A mandatory test runs and fails, thus the test *execution* job is green but
   the result gathering job is marked as failed.
1. The maintainer looks into the failure either via the presented [failed test]
   view on the MR or via the pipeline job logs.
1. The maintainer (working with the test maintainer, if needed) determines the
   test failure is unrelated to their changes.
1. The maintainer clicks the "waive failure here" link and fills in the information
   about the waive / failure.
1. The addition of a new waiver triggers an automatic restart of the relevant
   test result gathering job.
1. If that failure was the only blocking failure in the run, the pipeline is
   now marked as passing and doesn't block merging.

### Automatic restarts

We need a bidirectional hook between GitLab and test result dashboard / executor
/ whatever the name of the thing will be:

* If an autorestart waiver is present for the matching test/project/failure pattern,
  the execution GitLab job should get restarted. That should restart the test (see
  previous point).
* If a matching failure pattern (automatic or MR specific) is added, the result
  checking job should be automatically restarted.
* If all test execution jobs are finished again (e.g. after some were restarted),
  the result checking job should be automatically restarted to get updated results.

We expect the test executor to have an API endpoint for (re)starting the test.
If this is not going to be available, the webhook is also needed to start and
restart tests when the appropriate GitLab test job is (re)started. For design
simplicity from both sides, the API in the executor is needed and the prototype
plays with this idea.

### Test fixes should not cause a pipeline or build restart

The pipeline definition YAML is parsed at the time of the pipeline creation. Thus,
if the pipeline fails due to a bug in it, it gets fixed, and a *job* is restarted,
it will fail the same way. One has to start a whole new pipeline to get the new
pipeline code. This is nice to provide reproducible pipeline runs, but is not
ideal if we need to pull in pipeline fixes and don't want to rerun the previously
successfully executed pipeline parts. E.g. a bug in test result querying shouldn't
force us to rebuild the package and rerun all tests again.

There are several ways to work around this, the easiest is to provide scripts
responsible for that job as those can get updated easily. A job restart would
download a new version of the script and get the fixes. These scripts can live
in the same location as the CI files do, they just need to be separate from the
YAML.

[if only optional tests failed]: https://docs.gitlab.com/ee/ci/yaml/#allow_failureexit_codes
[failed test]: https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html

## Final pipeline design approach

Check the "Automatic restarts" section for automation details. They are left
out of the flow below.

1. Submit a draft build.
1. A job responsible for test execution jobs runs. It depends on successful
   build. This job parses local test definitions, combines them with common tests
   and spawns a dynamic pipeline for each of them. Depending on how the tests
   are implemented (native GitLab CI test runs or external ones), that's how the
   internals of the test jobs will look like, so we can switch them around as
   we wish. The reason for spawning dynamic pipeline jobs for common tests is
   consistency: it is easier for maintainers to understand that all tests are
   spawned the same way and to use the same approach for looking into all results.
1. Test execution jobs run. If they succeed, all is good, if not, the executions
   need to be looked into and restarted once fixed. This is the ideal time to
   start optional jobs, but they can be started afterwards as well if the
   maintainer wants to save resources on them and only run them once all the
   rest passed.
1. Once executions are complete, a result checking job runs. This queries the
   executor / result DB / whatever thingy and provides the results in xunit
   format (to be viewable in the MR view) and prints them in the job. If automatic
   waivers are already done, they are taken into account.
1. If a test fails, links to specific logs and waivers are provided in the job
   logs and xunit. The job is marked as a failure and blocks the merge.
1. If the failed test is optional, all the same information is provided. If
   it's only optional tests that failed, the pipeline is marked as failed with
   warnings / allowed to fail. This does not block the merge.
