spec:
  inputs:
    included_pipeline_ref:
      default: 'main'
---

.with_artifacts:
  artifacts:
    when: always
    reports:
      dotenv:
        - metadata.env  # Information to pass along as needed, e.g. build ID and link
    expire_in: 6 months

# Template for handling retries for weird GitLab failures. This has nothing to do
# with retries for test execution issues.
.with_retries:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
      - unknown_failure
      - api_failure

.merge_check_gitbz:
  image: registry.gitlab.com/redhat/centos-stream/ci-cd/dist-git-gating-tests/check_bz:35
  tags:
    - redhat
    - gitbz
  script:
    - /usr/local/bin/check_gitbz || exit_code=$?
    - |
      if [ $exit_code -eq 0 ]; then
        echo -e "\e[0;102mThis change is approved for merging\e[0m"
      elif [ $exit_code -eq 77 ]; then
        echo -e "\e[0;103mThis change is being allowed without approval during the active development phase\e[0m"
      else
        echo -e "\e[41mThis change does not have the requisite ticket approvals. See above for details.\e[0m"
      fi
    - exit $exit_code
  allow_failure:
    exit_codes:
      - 77
  artifacts:
    paths:
      - "debug.log"
    when: always
    expire_in: 1 week
  rules:
    - if: '($CI_MERGE_REQUEST_EVENT_TYPE == "merge_train" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^(c8s|c9s|c10s)$/) || ($CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^(c8s|c9s|c10s)$/)'

.mr_comment_and_label:
  artifacts:
    paths:
      - environment.log
  allow_failure:
    exit_codes:
      - 77
  extends: [.with_retries]
  rules:
    # No need to sync on merge trains.
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/rog:4e79f4f6"
  script:
    - |
      MR_COMMENT="<ol><li>Do not forget to review this MR carefully if it comes from an external contributor.</li><li>Set downstream target by setting MR label as follows: <ul><li><code>target::latest</code>: latest y-stream (set by default)</li><li><code>target::zstream</code>: z-stream</li><li><code>target::exception</code>: exception</li></ul>You can use the <code>/label</code> command in a comment to set label.<br/>Do not forget to restart the pipeline after updating labels.</li><li>To start testing and gating of this MR, go the pipeline linked at the beginning of the page and run the <b>start</b> job.<br/><img src=\"https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines/-/raw/main/rog/start_pipeline.png\"/></li></ol>"
    - exit_code=0
    - /usr/local/bin/check_gitbz || exit_code=$?
    - |
      if [ $exit_code -eq 0 ]; then
        echo -e "\e[0;102mThis change is approved for merging\e[0m"
      elif [ $exit_code -eq 77 ]; then
        echo -e "\e[0;103mThis change is being allowed without approval during the active development phase\e[0m"
        MR_COMMENT=$(echo $MR_COMMENT | sed 's/^/<h4>This change is being allowed without approval during the active development phase.<\/h4><\/br>/')
      else
        echo -e "\e[41mThis change does not have the requisite ticket approvals. See above for details.\e[0m"
        MR_COMMENT=$(echo $MR_COMMENT | sed 's/^/<h4>This change does not have the requisite ticket approvals. See console for details. Merge not allowed.<\/h4><\/br>/')
      fi
    - export GITLAB_TOKEN=$(cat /opt/secrets/gitlab.token)
    - export GLAB_CONFIG_DIR=/tmp
    - |
      if [ ! "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        HAS_CONFLICTS=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID} | jq ".has_conflicts")
        if [ "$HAS_CONFLICTS" = "false" ]; then
          echo "CI_MERGE_REQUEST_SOURCE_BRANCH_SHA undefined, falling back to CI_COMMIT_SHA"
          echo "This is a bug, proceed with caution and review the environment.log artifact file."
          glab mr note -m "<h4>CI_MERGE_REQUEST_SOURCE_BRANCH_SHA undefined, falling back to CI_COMMIT_SHA<br/>This is a bug, proceed with caution.<br/>Saving environment artifact for debugging.</h4>" -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
        else
          glab mr note -m "<h4>WARNING: Merged result pipeline not possible, build stage will fail. Please rebase and resolve conflicts.</h4>" -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
        fi
      fi
    - export | tee environment.log
    - >
      glab mr note -m "$MR_COMMENT" --unique -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
    - >
      if [ -z "$CI_MERGE_REQEST_LABELS" ]; then curl -X PUT --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" --data '{"labels": "target::latest"}' https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}; fi
    - exit $exit_code

.zstream_mr_comment:
  artifacts:
    paths:
      - environment.log
  # TODO: Allow failure for now to unblock work on prototyping other jobs.
  allow_failure: true
  extends: [.with_retries]
  rules:
    # No need to sync on merge trains.
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/rog:4e79f4f6"
  script:
    - |
      MR_COMMENT="To start testing and gating of this MR, go the pipeline linked at the beginning of the page and run the <b>start</b> job.<br/><img src=\"https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines/-/raw/main/rog/start_pipeline.png\"/>"
    - exit_code=0
    - /usr/local/bin/check_gitbz || exit_code=$?
    - |
      if [ $exit_code -eq 0 ]; then
        echo -e "\e[0;102mThis change is approved for merging\e[0m"
      elif [ $exit_code -eq 77 ]; then
        echo -e "\e[0;103mThis change is being allowed without approval during the active development phase\e[0m"
        MR_COMMENT=$(echo $MR_COMMENT | sed 's/^/<h4>This change is being allowed without approval during the active development phase.<\/h4><\/br>/')
      else
        echo -e "\e[41mThis change does not have the requisite ticket approvals. See above for details.\e[0m"
        MR_COMMENT=$(echo $MR_COMMENT | sed 's/^/<h4>This change does not have the requisite ticket approvals. See console for details. Merge not allowed.<\/h4><\/br>/')
      fi
    - export GITLAB_TOKEN=$(cat /opt/secrets/gitlab-private.token)
    - export GLAB_CONFIG_DIR=/tmp
    - |
      if [ ! "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        HAS_CONFLICTS=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID} | jq ".has_conflicts")
        if [ "$HAS_CONFLICTS" = "false" ]; then
          echo "CI_MERGE_REQUEST_SOURCE_BRANCH_SHA undefined, falling back to CI_COMMIT_SHA"
          echo "This is a bug, proceed with caution and review the environment.log artifact file."
          glab mr note -m "<h4>CI_MERGE_REQUEST_SOURCE_BRANCH_SHA undefined, falling back to CI_COMMIT_SHA<br/>This is a bug, proceed with caution.<br/>Saving environment artifact for debugging.</h4>" -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
        else
          glab mr note -m "<h4>WARNING: Merged result pipeline not possible, build stage will fail. Please rebase and resolve conflicts.</h4>" -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
        fi
      fi
    - >
      glab mr note -m "$MR_COMMENT" --unique -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
    - export | tee environment.log

.start:
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/rog:4e79f4f6"
  rules:
    # Don't rebuild the image on merge trains! We want to promote the existing one
    # in that case
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  when: manual
  artifacts:
    reports:
      dotenv:
        - metadata.env
  script:
    - echo "Starting build"
    - echo "Checking side-tag"
    - export GITLAB_TOKEN=$(cat /opt/secrets/gitlab.token)
    - |
      MR_JSON=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID})
    - |
      SIDE_TAG=$(echo $MR_JSON | jq -r '.description' | grep '^side-tag:' || :)
    - |
      if [ -n "$SIDE_TAG" ]; then
        SIDE_TAG=$(echo $SIDE_TAG | sed 's/^.*: \?//')  # Remove "side-tag: " prefix
      fi
      echo "SIDE_TAG=$SIDE_TAG" >> metadata.env

.sync_mr_downstream:
  extends: [.with_artifacts, .with_retries]
  needs:
    - job: start
      artifacts: true
  artifacts:
    reports:
      dotenv:
        - metadata.env
  rules:
    # No need to sync on merge trains.
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  image: images.paas.redhat.com/osci/base-minimal:901cda3c
  tags:
    - distrobaker
  script:
    - export
    - RHEL_OR_STREAM=$(basename $(dirname $CI_PROJECT_NAMESPACE))
    - |
      if [[ "$RHEL_OR_STREAM" == "rhel" ]]; then
        VISIBILITY="private"
      elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]]; then
        VISIBILITY="public"
      else
        echo "MR is not in the Centos Stream or RHEL namespace, exiting"
        exit 1
      fi
    - TYPE=$(basename $CI_PROJECT_NAMESPACE)
    - echo "https://distrogitsync.osci.redhat.com/${VISIBILITY}_$TYPE/$TYPE/$CI_PROJECT_NAME/mr/$CI_MERGE_REQUEST_IID"
    - CONTENT_LENGTH=$(curl --fail-with-body -X POST "https://distrogitsync.osci.redhat.com/${VISIBILITY}_$TYPE/$TYPE/$CI_PROJECT_NAME/mr/$CI_MERGE_REQUEST_IID" -i | grep "content-length" | tr -d -c 0-9)
    # If content length is zero something has gone wrong and we need to fail.
    - |
      if [ $CONTENT_LENGTH -eq 0 ] ; then
        echo "distrogitsync responded with zero-length response. Something is wrong, failing job."
        exit 1
      fi
    - echo "SIDE_TAG=$SIDE_TAG" >> metadata.env

.build_rpm:
  artifacts:
    when: always
    paths:
      - distrobuildsync.html
    reports:
      dotenv:
        - metadata.env  # Information to pass along as needed, e.g. build ID and link
    expire_in: 6 months
    expose_as: 'Build Brew RPM log'
  extends: [.with_retries]
  needs:
    - job: sync_mr_downstream
      artifacts: true
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/distrobuildsync:distrobuildsync-65c5419f"
  rules:
    # Don't rebuild the image on merge trains! We want to promote the existing one
    # in that case
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  script:
    # - echo "build_link=https://koji/123" >> metadata.env
    # - echo "build_id=123" >> metadata.env
    - echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - echo "SIDE_TAG=$SIDE_TAG"
    - kinit -k -t "/opt/secrets/distrobaker.keytab" "distrobaker/distrobaker.osci.redhat.com@REDHAT.COM"
    # Get the build target.
    - RHEL_OR_STREAM=$(basename $(dirname $CI_PROJECT_NAMESPACE))
    - |
      if [[ "$RHEL_OR_STREAM" == "rhel" ]]; then
        # Find leading digit(s) - RHEL major version
        X=$(grep -o '[0-9]\{1,2\}' <<< $CI_MERGE_REQUEST_TARGET_BRANCH_NAME | head -1)
        PROFILE="c${X}s"
        TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-z-candidate
      elif [[ "$RHEL_OR_STREAM" == "centos-stream" ]]; then
        PROFILE=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      else
        echo "MR is not in the Centos Stream or RHEL namespace, exiting"
        exit 1
      fi
    - NS_COMPONENT=$(basename "$(dirname "$CI_MERGE_REQUEST_PROJECT_PATH")")/$(basename "$CI_MERGE_REQUEST_PROJECT_PATH")
    - IFS=',' read -r -a labels <<< "$CI_MERGE_REQUEST_LABELS"
    - |
      for label in "${labels[@]}"
      do
          if [[ $label == target::* ]]; then
              TARGET_LABEL="${label#target::}"
              break
          fi
      done
    - |
      if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        REF=$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
      else
        REF=$CI_COMMIT_SHA
      fi
    - echo "Submitting brew build of ${NS_COMPONENT}#${REF}, using distrobuildsync profile $PROFILE, target $TARGET, target label $TARGET_LABEL"
    # TODO: Remove the -l debug once we know it works as expected
    # The -t and -p are used to specify build target.
    # If $TARGET is non-empty, it's used.
    # If $TARGET is not specified (empty) distrobuildsync uses target specified in the distrobaker config for the profile (-p $PROFILE).
    - python3 /tmp/distrobuildsync -l debug -b "${NS_COMPONENT}#${REF}" -t "$TARGET" -p "$PROFILE" -i "${TARGET_LABEL:-latest}" https://gitlab.cee.redhat.com/osci/distrobaker_config.git#rhel9 2>&1 | tee distrobuildsync.log
    - echo "<html><body><h1>Distrobuildsync build log</h1><pre>" > distrobuildsync.html
    - cat distrobuildsync.log >> distrobuildsync.html
    - echo "</pre></body></html>" >> distrobuildsync.html
    # : INFO : Build submitted for rpms/cups, target rhel-9.5.0-candidate; task 60118525; SCMURL: git+https://pkgs.devel.redhat.com/git/rpms/cups#4aa5ef7...
    - TASK_ID=$(tail distrobuildsync.log | grep "Build submitted for " | grep -Eo ' task [0-9]+' | awk '{ print $2 }')
    - TARGET_SUBMITTED=$(tail distrobuildsync.log | grep 'Build submitted for ' | grep -Eo ' target [^;]+' | awk '{ print $2 }')
    # INFO : Processing build for rpms/NetworkManager, build info { ...  'nvr': 'NetworkManager-1.47.5-2.el10', 'rhel_target': 'latest' ...
    - |
      NVR=$(grep 'Processing build for ' distrobuildsync.log | grep -o " 'nvr': [^,]\+" || :)
    - |
      NVR=$(sed "s/.*: '//;s/'$//" <<< $NVR)
    - echo "NVR=$NVR" >> metadata.env
    - echo "KOJI_TASK_ID=$TASK_ID" >> metadata.env
    - echo "KOJI_BUILD_TARGET=$TARGET_SUBMITTED" >> metadata.env
    - brew watch-task "$TASK_ID"
    # - "koji build --wait" or whatever executed on the background
    # - retrieve link to build and print
    # GitLab can get unhappy if there is no output on the console for a long while
    # - while pgrep brew ; do sleep 60; echo -ne "\0" ; done
    # - check build status and pass or fail with appropriate message
    - echo "Everything went well!"

.build_centos_stream_rpm:
  extends: [.with_artifacts, .with_retries]
  needs:
    - job: start
      artifacts: true
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/distrobuildsync:distrobuildsync-636b0e47"
  rules:
    # Don't rebuild the image on merge trains! We want to promote the existing one
    # in that case
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  script:
    - kinit -k -t "/opt/secrets/distrobaker.keytab" "distrobaker/distrobaker.osci.redhat.com@REDHAT.COM"
    - echo "Submitting koji scratch-build!"
    - NS_COMPONENT=$(basename "$(dirname "$CI_MERGE_REQUEST_PROJECT_PATH")")/$(basename "$CI_MERGE_REQUEST_PROJECT_PATH")
    # scoped labels are mutually exclusive so no need for if-else
    - |
      if [ -n "$SIDE_TAG" ]; then
        TARGET=$SIDE_TAG
      else
        TARGET=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-candidate
      fi
    - |
      echo "koji build target: $TARGET"
    - RHEL_TARGET=$(echo $CI_MERGE_REQUEST_LABELS | grep -o 'target::\w\+' | sed 's/.*://')
    - |
      if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        REF=$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
      else
        REF=$CI_COMMIT_SHA
      fi
    - |
      koji -p stream build --fail-fast --nowait --scratch --custom-user-metadata "{\"rhel-target\": \"$RHEL_TARGET\"}" $TARGET "git+https://gitlab.com/redhat/centos-stream/${NS_COMPONENT}.git#${REF}" | tee koji_build.log
    - TASK_ID=$(cat koji_build.log | grep "^Created task:\ " | awk '{ print $3 }')
    - echo "KOJI_TASK_ID=$TASK_ID" >> metadata.env
    - echo "KOJI_BUILD_TARGET=$TARGET" >> metadata.env
    - koji -p stream watch-task "$TASK_ID"
    - echo "Everything went well!"

.generate_test_jobs:
  extends: [.with_retries]
  tags:
    - distrobaker
  image: images.paas.redhat.com/osci/rog:4e79f4f6
  rules:
    # No need to sync on merge trains.
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  artifacts:
    when: always
    paths:
      - test_runs.yml  # Generated dynamic pipeline
      - metadata.env  # Information to pass along as needed, e.g. build ID and link
    reports:
      dotenv:
        - metadata.env  # Information to pass along as needed, e.g. build ID and link
    expire_in: 6 months
  needs:
    - job: build_rpm
      optional: true
      artifacts: true
  script:
    - |
      if [ $[[ inputs.included_pipeline_ref ]] = "main" ]; then
        git clone --depth 1 https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines.git .rog-pipeline
        echo "Production run, shallow-cloning test stage generator"
      else
        git clone https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines.git .rog-pipeline
        echo "Testing MR run, full-cloning test stage generator"
      fi
    - echo "Using ref $[[ inputs.included_pipeline_ref ]]."
    - cd .rog-pipeline
    - git checkout $[[ inputs.included_pipeline_ref ]]
    - cd ..
    - echo "running python script that generates test jobs based on gating setup"
    - ./.rog-pipeline/./rog/scripts/generate-test-stage.py "rpms/$CI_PROJECT_NAME"  # Creates test_runs.yaml.
    - |
      if [ "$SIDE_TAG" ]; then
        PATTERN=$(echo $NVR | sed 's/\./\\./g')
        ADDITIONAL_ARTIFACTS=$(brew list-tagged --quiet --latest $SIDE_TAG | cut -d ' ' -f 1 | grep -v "$PATTERN" | sed -z 's/\n/,/g')
      fi
    - echo "SIDE_TAG=$SIDE_TAG" >> metadata.env
    - echo "KOJI_TASK_ID=$KOJI_TASK_ID" >> metadata.env
    - echo "ADDITIONAL_ARTIFACTS=$ADDITIONAL_ARTIFACTS" >> metadata.env

.trigger_tests:
  rules:
    # No need to sync on merge trains.
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE != "merge_train" && $CI_PIPELINE_SOURCE == "merge_request_event"'
  needs:
    - job: generate_test_jobs
      artifacts: true
  trigger:
    include:
      - artifact: test_runs.yml
        job: generate_test_jobs
    strategy: depend
  variables:
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID  # So the test pipelines know where to get metadata from
    SIDE_TAG: $SIDE_TAG
    KOJI_TASK_ID: $KOJI_TASK_ID
    ADDITIONAL_ARTIFACTS: $ADDITIONAL_ARTIFACTS

.sync_downstream:
  extends: [.with_artifacts, .with_retries]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" || $CI_MERGE_REQUEST_EVENT_TYPE == "merge_train"'
  tags:
    - distrobaker
  image: images.paas.redhat.com/osci/base-minimal:901cda3c
  needs: [merge_check_gitbz]
  script:
    - export
    - NS_COMPONENT=$(basename "$(dirname "$CI_PROJECT_PATH")")/$(basename "$CI_PROJECT_PATH")
    - |
      if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        REF=$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
      else
        REF=$CI_COMMIT_SHA
    - curl --fail-with-body -X POST "distrogitsync.osci.redhat.com/$NS_COMPONENT/$REF"

.promote_build:
  extends: [.with_artifacts, .with_retries]
  rules:
    # Only promote this build when we click the merge button
    - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH =~ /^(c8s|c9s|c10s)$/'
  script:
    - echo "Checking the build source commit hash is synced to dist-git"
    # Get the RHEL major version from the Stream branch
    - MAJOR=$(echo $CI_COMMIT_BRANCH | tr -d -c 0-9)
    # Inspect dist-git branch to see if the new HEAD commit is the source commit for the build.
    - DOWNSTREAM_SHA=$(git ls-remote https://pkgs.devel.redhat.com/git/${NS_COMPONENT}/ rhel-${MAJOR}-main | cut -f1)
    - |
      if [ -n "$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA" ]; then
        REF=$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA
      else
        REF=$CI_COMMIT_SHA
      fi
    - |
      if [[ "$DOWNSTREAM_SHA" != "$REF" ]]; then
        echo "The source commit for this draft build has not been synced downstream - not promoting this build."
        exit 1
    - echo "Using Brew API to promote draft build to production"
    - curl -s -o promote_build.py https://gitlab.com/redhat/centos-stream/ci-cd/osci-pipelines/-/raw/main/rog/promote_build.py
    - kinit -k -t "/opt/secrets/distrobaker.keytab" "distrobaker/distrobaker.osci.redhat.com@REDHAT.COM"
    - ./promote_build.py

.approve_mr:
  extends: [.with_retries]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_MERGE_REQUEST_EVENT_TYPE != "merge_train"'
  tags:
    - distrobaker
  image: "images.paas.redhat.com/osci/rog:4e79f4f6"
  needs: [trigger_tests]
  # We need to allow this job to fail so that a human can approve instead of this job if required.
  allow_failure: true
  script:
    - export GITLAB_TOKEN=$(cat /opt/secrets/gitlab.token)
    - export GLAB_CONFIG_DIR=/tmp
    - |
      PIPELINE_BRIDGES=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/bridges)
    - |
      TEST_STAGE_PIPELINE_ID=$(echo $PIPELINE_BRIDGES | jq -r '.[0].downstream_pipeline.id')
    - |
      TEST_STAGE_PIPELINE=$(curl -s --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --header "Content-Type: application/json" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${TEST_STAGE_PIPELINE_ID})
    - |
      TEST_STAGE_PIPELINE_STATUS=$(echo $TEST_STAGE_PIPELINE | jq -r '.detailed_status.text')
    - |
      if [[ "$TEST_STAGE_PIPELINE_STATUS" == "Passed" ]]; then
        echo "Test stage pipeline passed - approving the MR."
        glab mr approve -R "$CI_PROJECT_URL" "$CI_MERGE_REQUEST_IID"
      elif [[ "$TEST_STAGE_PIPELINE_STATUS" == "Warning" ]]; then
        echo "Test stage pipeline has warnings - one or more tests has failed - NOT approving the MR."
        curl -s --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/discussions?body=One%20or%20more%20tests%20did%20not%20succeed.%20Please%20review%20the%20test%20pipeline%20and%20resolve%20the%20issues.%20If%20you%20think%20the%20test%20failures%20are%20false%20positives%20please%20provide%20your%20justification%20in%20reply%20to%20this%20thread.%20Closing%20all%20unresolved%20threads%20will%20allow%20an%20approved%20maintainer%20to%20approve%20and%20merge%20this%20MR.
        exit 1
      fi
